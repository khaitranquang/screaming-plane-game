export default class Game {
    

    constructor() {
        this.speed = 0;
        this.mode = "normalMode";
        this.initSpeed = .00035;
        this.baseSpeed = .00035;
        this.targetBaseSpeed = .00035;
        this.incrementSpeedByTime = .0000025;
        this.incrementSpeedByLevel = .000005;
        this.distanceForSpeedUpdate = 100;
        this.speedLastUpdate = 0;

        this.distance = 0;
        this.ratioSpeedDistance = 50;
        this.energy = 100;
        this.ratioSpeedEnergy = 3;

        this.level = 1;
        this.levelLastUpdate = 0;
        this.distanceForLevelUpdate = 1000;

        this.planeDefaultHeight = 100;
        this.planeAmpHeight = 80;
        this.planeAmpWidth = 75;
        this.planeMoveSensivity = 0.005;
        this.planeRotXSensivity = 0.0008;
        this.planeRotZSensivity = 0.0004;
        this.planeFallSpeed = .001;
        this.planeMinSpeed = 1.2;
        this.planeMaxSpeed = 1.6;
        this.planeSpeed = 0;
        this.planeCollisionDisplacementX = 0;
        this.planeCollisionSpeedX = 0;

        this.planeCollisionDisplacementY = 0;
        this.planeCollisionSpeedY = 0;

        this.seaRadius = 600;
        this.seaLength = 800;
            //seaRotationSpeed=0.006;
        this.wavesMinAmp = 5;
        this.wavesMaxAmp = 20;
        this.wavesMinSpeed = 0.001;
        this.wavesMaxSpeed = 0.003;

        this.cameraFarPos = 500;
        this.cameraNearPos = 150;
        this.cameraSensivity = 0.002;

        this.coinDistanceTolerance = 15;
        this.coinValue = 3;
        this.coinsSpeed = .5;
        this.coinLastSpawn = 0;
        this.distanceForCoinsSpawn = 100;

        this.enemyDistanceTolerance = 10;
        this.enemyValue = 10;
        this.enemiesSpeed = .6;
        this.enemyLastSpawn = 0;
        this.distanceForEnnemiesSpawn = 50;
        // this.status = "playing"
        this.status = "choseMode"
    }

    // Reset game - Set all variables to default value
    resetGame() {
        this.speed = 0;
        this.initSpeed = .00035;
        this.baseSpeed = .00035;
        this.targetBaseSpeed = .00035;
        this.incrementSpeedByTime = .0000025;
        this.incrementSpeedByLevel = .000005;
        this.distanceForSpeedUpdate = 100;
        this.speedLastUpdate = 0;

        this.distance = 0;
        this.ratioSpeedDistance = 50;
        this.energy = 100;
        this.ratioSpeedEnergy = 3;

        this.level = 1;
        this.levelLastUpdate = 0;
        this.distanceForLevelUpdate = 1000;

        this.planeDefaultHeight = 100;
        this.planeAmpHeight = 80;
        this.planeAmpWidth = 75;
        this.planeMoveSensivity = 0.005;
        this.planeRotXSensivity = 0.0008;
        this.planeRotZSensivity = 0.0004;
        this.planeFallSpeed = .001;
        this.planeMinSpeed = 1.2;
        this.planeMaxSpeed = 1.6;
        this.planeSpeed = 0;
        this.planeCollisionDisplacementX = 0;
        this.planeCollisionSpeedX = 0;

        this.planeCollisionDisplacementY = 0;
        this.planeCollisionSpeedY = 0;

        this.seaRadius = 600;
        this.seaLength = 800;
        this.wavesMinAmp = 5;
        this.wavesMaxAmp = 20;
        this.wavesMinSpeed = 0.001;
        this.wavesMaxSpeed = 0.003;

        this.cameraFarPos = 500;
        this.cameraNearPos = 150;
        this.cameraSensivity = 0.002;

        this.coinDistanceTolerance = 15;
        this.coinValue = 3;
        this.coinsSpeed = .5;
        this.coinLastSpawn = 0;
        this.distanceForCoinsSpawn = 100;

        this.enemyDistanceTolerance = 10;
        this.enemyValue = 10;
        this.enemiesSpeed = .6;
        this.enemyLastSpawn = 0;
        this.distanceForEnnemiesSpawn = 50;
        this.status = "choseMode"
    }

    getMode() {
        return this.mode;
    }
    setMode(mode) {
        this.mode = mode;
    }

    getCoinLastSpawn() {
        return this.coinLastSpawn;
    }
    setCoinLastSpawn(coinLastSpawn) {
        this.coinLastSpawn = coinLastSpawn;
    }

    getDistanceForSpeedUpdate() {
        return this.distanceForSpeedUpdate;
    }

    getSeaLength() {
        return this.seaLength;
    }
    getWavesMinAmp(){
        return this.wavesMinAmp;
    }
    getWavesMaxAmp() {
        return this.wavesMaxAmp;
    }
    getWavesMinSpeed() {
        return this.wavesMinSpeed;
    }
    getWavesMaxSpeed() {
        return this.wavesMaxSpeed;
    }
    setBaseSpeed(baseSpeed) {
        this.baseSpeed = baseSpeed;
    }
    getBaseSpeed() {
        return this.baseSpeed;
    }
    getTargetBaseSpeed(){
        return this.targetBaseSpeed;
    }

    getSpeedLastUpdate() {
        return this.speedLastUpdate;
    }
    setSpeedLastUpdate(speedLastUpdate) {
        this.speedLastUpdate = speedLastUpdate;
    }
    setTargetBaseSpeed(targetBaseSpeed) {
        this.targetBaseSpeed = targetBaseSpeed;
    }
    getIncrementSpeedByTime(){
        return this.incrementSpeedByTime;
    }
    getPlaneSpeed() {
        return this.planeSpeed;
    }

    getRatioSpeedEnergy() {
        return this.ratioSpeedEnergy;
    }

    getPlaneFallSpeed() {
        return this.planeFallSpeed;
    }
    setPlaneFallSpeed(planeFallSpeed) {
        this.planeFallSpeed = planeFallSpeed;
    }

    setStatus(status) {
        this.status = status;
    }

    getEnergy(){
        return this.energy;
    }
    setEnergy(energy) {
        this.energy = energy;
    }
    setSpeed(speed) {
        this.speed = speed;
    }
    setDistance(distance) {
        this.distance = distance;
    }
    getRatioSpeedDistance() {
        return this.ratioSpeedDistance;
    }

    getDistanceForEnnemiesSpawn() {
        return this.distanceForEnnemiesSpawn;
    }
    getEnemyLastSpawn() {
        return this.enemyLastSpawn;
    }
    setEnemyLastSpawn(enemyLastSpawn) {
        this.enemyLastSpawn = enemyLastSpawn;
    }

    getDistanceForLevelUpdate() {
        return this.distanceForLevelUpdate;
    }
    getLevelLastUpdate() {
        return this.levelLastUpdate;
    }
    setLevelLastUpdate(levelLastUpdate) {
        this.levelLastUpdate = levelLastUpdate;
    }
    setLevel(level) {
        this.level = level;
    }

    getInitSpeed(){
        return this.initSpeed;
    }
    getIncrementSpeedByLevel() {
        return this.incrementSpeedByLevel;
    }


    getDistanceForCoinsSpawn() {
        return this.distanceForCoinsSpawn;
    }
    getDistance() {
        return this.distance;
    }
    getStatus() {
        return this.status;
    }
    getLevel(){
        return this.level;
    }
    getSeaRadius() {
        return this.seaRadius;
    }
    getPlaneDefaultHeight() {
        return this.planeDefaultHeight;
    }
    getPlaneAmpHeight() {
        return this.planeAmpHeight;
    }
    getSpeed(){
        return this.speed;
    }
    getEnemiesSpeed() {
        return this.enemiesSpeed;
    }
    getEnemyDistanceTolerance() {
        return this.enemyDistanceTolerance;
    }
    getCoinsSpeed() {
        return this.coinsSpeed;
    }

    getCoinDistanceTolerance() {
        return this.coinDistanceTolerance;
    }
    setPlaneSpeed(planeSpeed) {
        this.planeSpeed = planeSpeed;
    }
    getPlaneMaxSpeed() {
        return this.planeMaxSpeed;
    }
    getPlaneAmpWidth() {
        return this.planeAmpWidth;
    }
    setPlaneCollisionDisplacementX(planeCollisionDisplacementX) {
        this.planeCollisionDisplacementX = planeCollisionDisplacementX;
    }
    setPlaneCollisionDisplacementY(planeCollisionDisplacementY) {
        this.planeCollisionDisplacementY = planeCollisionDisplacementY;
    }
    getPlaneCollisionDisplacementX() {
        return this.planeCollisionDisplacementX;
    }
    getPlaneCollisionDisplacementY() {
        return this.planeCollisionDisplacementY;
    }
    getPlaneMoveSensivity() {
        return this.planeMoveSensivity;
    }
    getPlaneRotXSensivity() {
        return this.planeRotXSensivity
    }
    getPlaneRotZSensivity () {
        return this.planeRotZSensivity;
    }
    getCameraSensivity() {
        return this.cameraSensivity;
    }

    getPlaneMinSpeed() {
        return this.planeMinSpeed;
    }
    getPlaneCollisionSpeedX() {
        return this.planeCollisionSpeedX;
    }
    getPlaneCollisionSpeedY() {
        return this.planeCollisionSpeedY;
    }
    setPlaneCollisionSpeedX(planeCollisionSpeedX) {
        this.planeCollisionSpeedX = planeCollisionSpeedX;
    }
    setPlaneCollisionSpeedY(planeCollisionSpeedY) {
        this.planeCollisionSpeedY = planeCollisionSpeedY;
    }
    removeEnergy() {
        this.energy -= this.enemyValue;
        this.energy = Math.max(0, this.energy);
    }
    addEnergy() {
        this.energy += this.coinValue;
        this.energy = Math.min(this.energy, 100);
    }

    hideReplay(replayMessage) {
        replayMessage.style.display="none";
    }
}
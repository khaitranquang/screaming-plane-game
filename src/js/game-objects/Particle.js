

export default class Particle {
    constructor() {
        let geom = new THREE.TetrahedronGeometry(3, 0);
        let mat = new THREE.MeshPhongMaterial({
            color: 0x009999,
            shininess: 0,
            specular: 0xffffff,
            shading: THREE.FlatShading
        });
        this.mesh = new THREE.Mesh(geom, mat);
    }

    getMesh() {
        return this.mesh;
    }

    setMesh(mesh) {
        this.mesh = mesh;
    }

    explode(pos, color, scale, particlesPool) {
        var _this = this;
        let _p =this.mesh.parent;

        this.mesh.material.color = new THREE.Color(color);
        this.mesh.material.needsUpdate = true;
        this.mesh.scale.set(scale, scale, scale);
        let targetX = pos.x + (-1 + Math.random()*2)*50;
        let targetY = pos.y + (-1 + Math.random()*2)*50;
        let speed = .6+Math.random()*.2;
        TweenMax.to(this.mesh.rotation, speed, {x:Math.random()*12, y:Math.random()*12});
        TweenMax.to(this.mesh.scale, speed, {x:.1, y:.1, z:.1});
        TweenMax.to(this.mesh.position, speed, {
            x:targetX,
            y:targetY,
            delay:Math.random() *.1,
            ease:Power2.easeOut,
            onComplete:function(){
                if(_p) _p.remove(_this.mesh);
                _this.mesh.scale.set(1,1,1);
                particlesPool.unshift(_this);
            }
        });
    }
}
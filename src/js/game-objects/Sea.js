/*
 * This class is Sea object
 */

'use strict';


import Colors from "../constants/Colors.js";


class Sea {
    constructor(game) {
        this.mesh = null;
        this.waves = [];
        this.init(game);
    }

    init(game) {
        // Create the geometry (shape) of the cylinder
        // The parameters are: radius top, radius bottom, height, number of segments on the radius, number of segments vertically
        let geom = new THREE.CylinderGeometry(game.getSeaRadius(), game.getSeaRadius(), game.getSeaLength(), 40, 10);

        // Rotate the geometry on the x axis
        geom.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));

        // By merging vertices we ensure the continuity of the waves
        geom.mergeVertices();
        // Get the vertices
        let l = geom.vertices.length;
        for (let i = 0; i < l; i++) {
            let vertex = geom.vertices[i];
            this.waves.push({
                y: vertex.y,
                x: vertex.x,
                z: vertex.z,
                ang: Math.random() * Math.PI * 2,       // Random angle
                amp: game.getWavesMinAmp() + Math.random()*(game.getWavesMaxAmp()-game.getWavesMinAmp()),            // Random distance
                speed: game.getWavesMinSpeed() + Math.random()*(game.getWavesMaxSpeed() - game.getWavesMinSpeed())    // A random speed between 0.016 and 0.048 radians / frame
            })
        }


        // Create the material
        let material = new THREE.MeshPhongMaterial({
            color: Colors.BLUE,
            transparent: true,
            opacity: .8,
            shading: THREE.FlatShading
        });

        // To create an object in Three.js, we have to create a mesh which is a combination of a geometry and some material
        this.mesh = new THREE.Mesh(geom, material);

        // Allow the sea to receive shadows
        this.mesh.receiveShadow = true;
    }

    moveWaves(deltaTime) {
        let verts = this.mesh.geometry.vertices;
        let l = verts.length;

        for (let i=0; i<l; i++){
            let v = verts[i];

            // get the data associated to it
            let vprops = this.waves[i];

            // update the position of the vertex
            v.x = vprops.x + Math.cos(vprops.ang)*vprops.amp;
            v.y = vprops.y + Math.sin(vprops.ang)*vprops.amp;

            // increment the angle for the next frame
            vprops.ang += vprops.speed*deltaTime;
            this.mesh.geometry.verticesNeedUpdate=true;

        }

        // Tell the renderer that the geometry of the sea has changed.
        // In fact, in order to maintain the best level of performance,
        // three.js caches the geometries and ignores any changes
        // unless we add this line
        // this.mesh.geometry.verticesNeedUpdate=true;
        //
        // this.mesh.rotation.z += .005;

    }

    getMesh() {
        return this.mesh;
    }
    setMesh(mesh) {
        this.mesh = mesh;
    }

    getWaves() {
        return this.waves;
    }
}


export default Sea;
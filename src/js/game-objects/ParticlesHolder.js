import Particle from "./Particle.js";

export default class ParticlesHolder {
    constructor(){
        this.mesh =new THREE.Object3D();
        this.particlesInUse = [];
    }

    getMesh() {
        return this.mesh;
    }
    setMesh(mesh) {
        this.mesh = mesh;
    }
    getParticlesInUse() {
        return this.particlesInUse;
    }
    spawnParticles(pos, density, color, scale, particlesPool) {
        let nParticles = density;
        for (let i=0; i<nParticles; i++){
            let particle;
            if (particlesPool.length) {
                particle = particlesPool.pop();
            }else{
                particle = new Particle();
            }
            let particleMesh = particle.getMesh();
            this.mesh.add(particleMesh);
            particleMesh.visible = true;
            particleMesh.position.y = pos.y;
            particleMesh.position.x = pos.x;
            particle.setMesh(particleMesh);
            particle.explode(pos,color, scale, particlesPool);
        }
    }
}
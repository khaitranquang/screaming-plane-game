/*
 * This class is Enemy game object
 */

import Colors from "../constants/Colors.js";


export default class Enemy {
    constructor(isFakeCoin) {
        let geom;
        if (isFakeCoin) {
            geom = new THREE.TetrahedronGeometry(5,0);
        }
        else {
            geom = new THREE.TetrahedronGeometry(8, 2);
        }
        // let geom = new THREE.TetrahedronGeometry(5,0);
        let mat = new THREE.MeshPhongMaterial({
            color: Colors.RED,
            shininess: 0,
            specular:0xffffff,
            shading:THREE.FlatShading
        });
        // let mat = new THREE.MeshPhongMaterial({
        //     shininess: 100,
        //     color: 0xffffff,
        //     specular: 0xffffff,
        //     transparent: true,
        //     side: THREE.BackSide,
        //     blending: THREE.AdditiveBlending,
        //     depthWrite: false
        // });
        this.mesh = new THREE.Mesh(geom, mat);
        this.mesh.castShadow = true;
        this.angle = 0;
        this.dist = 0;
    }

    getMesh() {
        return this.mesh;
    }
    setMesh(mesh) {
        this.mesh = mesh;
    }
    getAngle() {
        return this.angle;
    }
    setAngle(angle) {
        return this.angle = angle
    }
    getDist() {
        return this.dist;
    }
}
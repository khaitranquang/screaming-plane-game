/*
 * This class is Sky object
 * It contains multiple Cloud objects
 */

'use strict';

import Cloud from "./Cloud.js";


class Sky {
    constructor(numberOfClouds, game) {
        this.numberOfClouds = numberOfClouds;
        this.mesh = new THREE.Object3D();
        this.clouds = [];
        this.init(game);
    }

    init(game) {
        // To distribute the clouds consistently, need to place them according to a uniform angle
        let stepAngle = Math.PI * 2 / this.numberOfClouds;

        // Create the clouds
        for (let i=0; i < this.numberOfClouds; i++) {
            let cloud = new Cloud();
            this.clouds.push(cloud);
            let cloudMesh = cloud.getMesh();
            // Set the rotation and the position of each cloud
            // This is the final angle of the cloud
            let a = stepAngle * i;
            // This is the distance between the center of the axis and the cloud itself
            let h = game.getSeaRadius() + 150 + Math.random()*200;
            cloudMesh.position.x = Math.cos(a) * h;
            cloudMesh.position.y = Math.sin(a) * h;

            // Rotate the cloud according to its position
            cloudMesh.rotation.z = a + Math.PI / 2;

            // For a better result, we position the clouds at random depths inside of the scene
            cloudMesh.position.z = -400 - Math.random() * 400;
            // We also set a random scale for each cloud
            let s = 1 + Math.random() * 2;
            cloudMesh.scale.set(s, s, s);

            cloud.setMesh(cloudMesh);

            // Add the mesh of each cloud to sky scene
            this.mesh.add(cloud.getMesh());
        }
    }

    moveClouds(game, deltaTime) {
        for (let i = 0; i < this.numberOfClouds; i++) {
            let cloud =this.clouds[i];
            cloud.rotate();
        }
        this.mesh.rotation.z += game.getSpeed()*deltaTime;
    }

    getMesh() {
        return this.mesh;
    }
    setMesh(mesh) {
        this.mesh = mesh;
    }


}

export default Sky;
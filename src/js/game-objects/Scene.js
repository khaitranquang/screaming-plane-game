/*
 * This is scene of game
 */

'use strict';

class Scene {
    /**
     * Constructor of Scene class
     * @param container: Dom element to append scene
     * @param width: Width of scene
     * @param height: Height of scene
     * @param game: Game play object
     */
    constructor(container, width, height, game) {
        this.container = container;
        this.scene = null;
        this.camera = null;
        this.renderer = null;
        this.width = width;
        this.height = height;

        this.init(game);
    }

    init(game) {
        // Create new scene object from THREE js
        this.scene = new THREE.Scene();

        // Add a fog effect to the scene
        this.scene.fog = new THREE.Fog(0xf7d9aa, 100, 950);

        // Create new camera
        let aspectRatio = this.width / this.height,
            fieldOfView = 50,
            nearPlane = .1,
            farPlane = 10000;
        this.camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);

        // Set the position of the camera
        this.camera.position.x = 0;
        this.camera.position.z = 200;
        this.camera.position.y = game.getPlaneDefaultHeight();

        // Create renderer
        this.renderer = new THREE.WebGLRenderer({
            // Allow transparency to show the gradient background we defined in the CSS
            alpha: true,
            // Active the anti-aliasing
            antialias: true
        });
        this.renderer.setSize(this.width, this.height);

        // Enable shadow rendering
        this.renderer.shadowMap.enabled = true;

        // Add renderer to container
        this.container.appendChild(this.renderer.domElement);

    }

    getRenderer() {
        return this.renderer;
    }

    getCamera() {
        return this.camera;
    }

    getScene() {
        return this.scene;
    }
}

export default Scene;
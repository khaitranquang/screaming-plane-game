/*
 * This class is Light object for main Scene
 */


class Light{

    constructor() {
        this.hemisphereLight = null;
        this.shadowLight = null;
        this.ambientLight = null;
        this.init();
    }

    init() {
        this.ambientLight = new THREE.AmbientLight(0xdc8874, .5);

        // A hemisphere light is a gradient colored light;
        // the first parameter is the sky color, the second parameter is the ground color, the third parameter is the intensity of the light
        this.hemisphereLight = new THREE.HemisphereLight(0xaaaaaa,0x000000, .9);

        // A directional light shines from a specific direction
        // It acts like the sun, that means that all rays produced are parallel
        this.shadowLight = new THREE.DirectionalLight(0xffffff, .9);

        // Set the direction of the light
        this.shadowLight.position.set(150, 350, 350);

        // Allow shadow casting
        this.shadowLight.castShadow = true;

        // Define the visible area if the projected shadow
        this.shadowLight.shadow.camera.left = -400;
        this.shadowLight.shadow.camera.right = 400;
        this.shadowLight.shadow.camera.top = 400;
        this.shadowLight.shadow.camera.bottom = -400;
        this.shadowLight.shadow.camera.near = 1;
        this.shadowLight.shadow.camera.far = 1000;

        // Define the resolution of the shadow
        this.shadowLight.shadow.mapSize.width = 2048;
        this.shadowLight.shadow.mapSize.height = 2048;
    }

    getHemisphereLightDark() {
        this.hemisphereLight = new THREE.HemisphereLight(0x000000,0x000000, 1);
        return this.hemisphereLight;
    }
    getHemisphereLight() {
        // this.hemisphereLight = new THREE.HemisphereLight(0xaaaaaa,0x000000, .9);
        return this.hemisphereLight;
    }

    getShadowLightDark() {
        this.shadowLight = new THREE.DirectionalLight(0x000000, 1);
        return this.shadowLight;
    }
    getShadowLight() {
        // this.shadowLight = new THREE.DirectionalLight(0xffffff, .9);
        return this.shadowLight;
    }

    getAmbientLightDark() {
        this.ambientLight = new THREE.AmbientLight(0x000000, 1);
        return this.ambientLight;
    }
    getAmbientLight() {
        // this.ambientLight = new THREE.AmbientLight(0xdc8874, .5);
        return this.ambientLight;
    }
}

export default Light;
import Coin from "./Coin.js";


export default class CoinsHolder {
    constructor(numberOfCoins) {
        this.mesh = new THREE.Object3D();
        this.coinsInUse = [];
        this.coinsPool = [];
        for (let i=0; i<numberOfCoins; i++){
            let coin = new Coin();
            this.coinsPool.push(coin);
        }
    }

    getMesh() {
        return this.mesh;
    }
    setMesh(mesh) {
        this.mesh  = mesh;
    }
    getCoinsInUse() {
        return this.coinsInUse;
    }
    getCoinsPool() {
        return this.coinsPool;
    }

    spawnCoins(game) {
        let numberOfCoins = 1 + Math.floor(Math.random()*10);
        let d = game.getSeaRadius() + game.getPlaneDefaultHeight() + (-1 + Math.random() * 2) * (game.getPlaneAmpHeight()-20);
        let amplitude = 10 + Math.round(Math.random()*10);
        for (let i=0; i<numberOfCoins; i++){
            let coin;
            if (this.coinsPool.length) {
                coin = this.coinsPool.pop();
            }else{
                coin = new Coin();
            }
            let coinMesh = coin.getMesh();
            this.mesh.add(coinMesh);
            this.coinsInUse.push(coin);
            coin.setAngle(-(i*0.02));
            coin.distance = d + Math.cos(i*.5)*amplitude;
            coinMesh.position.y = -game.getSeaRadius() + Math.sin(coin.getAngle())*coin.distance;
            coinMesh.position.x = Math.cos(coin.getAngle())*coin.distance;
            coin.setMesh(coinMesh);
        }
    }

    rotateCoins(game, deltaTime, airplane, particlesHolder, particlesPool) {
        for (let i=0; i<this.coinsInUse.length; i++){
            let coin = this.coinsInUse[i];
            if (coin.exploding) {
                continue;
            }

            coin.setAngle(coin.getAngle() + game.getSpeed()*deltaTime*game.getCoinsSpeed());
            if (coin.getAngle()>Math.PI*2) {
                coin.setAngle(coin.getAngle()- Math.PI*2) ;
            }

            let coinMesh = coin.getMesh();
            coinMesh.position.y = -game.getSeaRadius() + Math.sin(coin.getAngle())*coin.distance;
            coinMesh.position.x = Math.cos(coin.getAngle())*coin.distance;
            coinMesh.rotation.z += Math.random()*.1;
            coinMesh.rotation.y += Math.random()*.1;
            coin.setMesh(coinMesh);

            let diffPos = airplane.getMesh().position.clone().sub(coin.getMesh().position.clone());
            let d = diffPos.length();
            if (d<game.getCoinDistanceTolerance()){
                this.coinsPool.unshift(this.coinsInUse.splice(i,1)[0]);
                this.mesh.remove(coin.getMesh());
                particlesHolder.spawnParticles(coin.mesh.position.clone(), 5, 0x009999, .8, particlesPool);
                game.addEnergy();
                i--;
            }else if (coin.angle > Math.PI){
                this.coinsPool.unshift(this.coinsInUse.splice(i,1)[0]);
                this.mesh.remove(coin.getMesh());
                i--;
            }
        }
    }
}
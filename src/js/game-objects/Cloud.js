/*
 * This class is cloud game object
 * Cloud is contains multiple cube objects
 */

'use strict';

import Colors from "../constants/Colors.js";


// class Colors {
//     static RED = 0xf25346;
//     static WHITE = 0xd8d0d1;
//     static BROWN = 0x59332e;
//     static PINK = 0xF5986E;
//     static BROWNDARK = 0x23190f;
//     static BLUE = 0x68c3c0;
// }




class Cloud {
    constructor() {
        this.mesh = null;
        this.init();
    }

    init() {

        // Create an empty container that will hold the different part of the cloud
        this.mesh = new THREE.Object3D();
        this.mesh.name ="cloud";

        // Create a cube geometry
        let geom = new THREE.CubeGeometry(20, 20, 20);

        // Create a material. A simple white material will do the trick
        let material = new THREE.MeshPhongMaterial({
            color: Colors.WHITE
        });

        // Duplicate the geometry a random number of times
        let nBlocs = 3 + Math.floor(Math.random() * 3);
        for (let i=0; i < nBlocs; i++) {
            // Create the mesh by cloning the geometry
            let m = new THREE.Mesh(geom, material);
            // Set the position and the rotation of each cube randomly
            m.position.x = i*15;
            m.position.y = Math.random()*10;
            m.position.z = Math.random()*10;
            m.rotation.z = Math.random()*Math.PI*2;
            m.rotation.y = Math.random()*Math.PI*2;

            // set the size of the cube randomly
            let s = .1 + Math.random()*.9;
            m.scale.set(s,s,s);

            // allow each cube to cast and to receive shadows
            m.castShadow = true;
            m.receiveShadow = true;

            // add the cube to the container we first created
            this.mesh.add(m);
        }
    }

    getMesh() {
        return this.mesh;
    }
    setMesh(cloudMesh) {
        this.mesh = cloudMesh;
    }

    rotate() {
        let l =this.mesh.children.length;
        for (let i = 0; i< l; i++) {
            let m = this.mesh.children[i];
            m.rotation.z+= Math.random()*.005*(i+1);
            m.rotation.y+= Math.random()*.002*(i+1);
        }
    }
}


export default Cloud;
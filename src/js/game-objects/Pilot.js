/*
 *  Pilot game object
 */

import Colors from "../constants/Colors.js";




class Pilot {
    constructor() {
        this.mesh = new THREE.Object3D();
        this.mesh.name = "pilot";
        this.angleHairs = 0;
        this.hairsTop = null;
        this.init();
    }

    init() {
        // Body
        let bodyGeom = new THREE.BoxGeometry(15, 15, 15);
        let bodyMat = new THREE.MeshPhongMaterial({
            color: Colors.BROWN,
            shading: THREE.FlatShading
        });
        let body = new THREE.Mesh(bodyGeom, bodyMat);
        body.position.set(2, -12, 0);
        this.mesh.add(body);

        // Face
        let faceGeom = new THREE.BoxGeometry(10, 10, 10);
        let faceMaterial = new THREE.MeshLambertMaterial({
            color: Colors.PINK
        });
        let face = new THREE.Mesh(faceGeom, faceMaterial);
        this.mesh.add(face);


        // Hair
        let hairGeom = new THREE.BoxGeometry(4, 4, 4);
        let hairMaterial = new THREE.MeshLambertMaterial({
            color: Colors.BROWN
        });
        let hair = new THREE.Mesh(hairGeom, hairMaterial);
        // Align the shape of the hair to its bottom boundary, that will make it easier to scale
        hair.geometry.applyMatrix(new THREE.Matrix4().makeTranslation(0, 2, 0));
        // Container for the hair
        let hairs = new THREE.Object3D();
        // Create container for the hairs at the top of the head (the ones that will be animated)
        this.hairsTop = new THREE.Object3D();
        // Create the hairs at the top of the head and position them on a 3x4 grid
        for (let i = 0; i < 12; i++) {
            let h = hair.clone();
            let col = i % 3;
            let row = Math.floor(i / 3);
            let startPosZ = -4;
            let startPosX = -4;
            h.position.set(startPosX + row * 4, 0, startPosZ + col * 4);
            this.hairsTop.add(h);
        }
        hairs.add(this.hairsTop);

        let hairSideGeom = new THREE.BoxGeometry(12, 4, 2);
        hairSideGeom.applyMatrix(new THREE.Matrix4().makeTranslation(-6, 0, 0));
        let hairSideR = new THREE.Mesh(hairSideGeom, hairMaterial);
        let hairSideL = hairSideR.clone();
        hairSideR.position.set(8,-2,6);
        hairSideL.position.set(8,-2,-6);
        hairs.add(hairSideR);
        hairs.add(hairSideL);

        // create the hairs at the back of the head
        let hairBackGeom = new THREE.BoxGeometry(2, 8, 10);
        let hairBack = new THREE.Mesh(hairBackGeom, hairMaterial);
        hairBack.position.set(-1, -4, 0);
        hairs.add(hairBack);
        hairs.position.set(-5, 5, 0);

        this.mesh.add(hairs);

        let glassGeom = new THREE.BoxGeometry(5, 5, 5);
        let glassMat = new THREE.MeshLambertMaterial({
            color: Colors.BROWN
        });
        let glassR = new THREE.Mesh(glassGeom, glassMat);
        glassR.position.set(6, 0, 3);
        let glassL = glassR.clone();
        glassL.position.z = -glassR.position.z;

        let glassAGeom = new THREE.BoxGeometry(11, 1, 11);
        let glassA = new THREE.Mesh(glassAGeom, glassMat);
        this.mesh.add(glassR);
        this.mesh.add(glassL);
        this.mesh.add(glassA);

        let earGeom = new THREE.BoxGeometry(2, 3, 2);
        let earL = new THREE.Mesh(earGeom, faceMaterial);
        earL.position.set(0, 0, -6);
        let earR = earL.clone();
        earR.position.set(0, 0, 6);
        this.mesh.add(earL);
        this.mesh.add(earR);
    }

    updateHairs(game, deltaTime) {
        let hairs = this.hairsTop.children;

        let l = hairs.length;
        for (let i = 0; i < l; i++) {
            let h = hairs[i];
            h.scale.y = .75 + Math.cos(this.angleHairs + i / 3) * .25;
        }
        this.angleHairs += game.getSpeed()*deltaTime*40;
    }

    getMesh() {
        return this.mesh;
    }

    getAngleHairs() {
        return this.angleHairs;
    }
}

export default Pilot;
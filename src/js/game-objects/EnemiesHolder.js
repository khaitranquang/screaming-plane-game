import Enemy from "./Enemy.js";
import Colors from "../constants/Colors.js";


export default class EnemiesHolder {
    constructor() {
        this.mesh = new THREE.Object3D();
        this.enemiesInUse = [];
    }

    getMesh() {
        return this.mesh;
    }

    getEnemiesInUse() {
        return this.enemiesInUse;
    }

    setMesh(mesh) {
        this.mesh = mesh;
    }

    spawnEnemies(game, enemiesPool) {
        let numberOfEnemies = game.getLevel();
        for (let i = 0; i < numberOfEnemies; i++) {
            let enemy;
            if (enemiesPool.length) {
                enemy = enemiesPool.pop();
            } else {
                enemy = new Enemy();
            }
            enemy.setAngle(-(i * 0.1));
            enemy.distance = game.getSeaRadius() + game.getPlaneDefaultHeight() + (-1 + Math.random() * 2) * (game.getPlaneAmpHeight() - 20);
            let enemyMesh = enemy.getMesh();
            enemyMesh.position.y = -game.getSeaRadius() + Math.sin(enemy.getAngle())*enemy.distance;
            enemyMesh.position.x = Math.cos(enemy.getAngle())*enemy.distance;
            enemy.setMesh(enemyMesh);

            this.mesh.add(enemy.getMesh());
            this.enemiesInUse.push(enemy);
        }
    }

    rotateEnemies(game, enemiesPool, deltaTime, airplane, particlesHolder, particlesPool, ambientLight) {
        for (let i = 0; i < this.enemiesInUse.length; i++) {
            let enemy = this.enemiesInUse[i];
            let newEnemyAngle = enemy.getAngle() + game.getSpeed()*deltaTime*game.getEnemiesSpeed();
            enemy.setAngle(newEnemyAngle);

            if (enemy.getAngle() > Math.PI*2) {
                enemy.setAngle(enemy.getAngle() - Math.PI*2);
            }
            // Rotate enemy by z and y axis
            let enemyMesh = enemy.getMesh();
            enemyMesh.position.y = -game.getSeaRadius() + Math.sin(enemy.getAngle())*enemy.distance;
            enemyMesh.position.x = Math.cos(enemy.getAngle())*enemy.distance;
            enemyMesh.rotation.z += Math.random()*.1;
            enemyMesh.rotation.y += Math.random()*.1;
            enemy.setMesh(enemyMesh);

            // Get diffPos of Air Plane and enemy
            let diffPos = airplane.getMesh().position.clone().sub(enemy.getMesh().position.clone());
            let d = diffPos.length();
            if (d<game.getEnemyDistanceTolerance()){
                particlesHolder.spawnParticles(enemy.getMesh().position.clone(), 15, Colors.RED, 3, particlesPool);

                enemiesPool.unshift(this.enemiesInUse.splice(i,1)[0]);
                this.mesh.remove(enemy.getMesh());
                game.setPlaneCollisionSpeedX(100 * diffPos.x / d);
                game.setPlaneCollisionSpeedY(100 * diffPos.y / d);
                ambientLight.intensity = 2;

                game.removeEnergy();
                i--;
            }
            else if (enemy.getAngle() > Math.PI){
                enemiesPool.unshift(this.enemiesInUse.splice(i,1)[0]);
                this.mesh.remove(enemy.getMesh());
                i--;
            }
        }
    }

}
class Colors {
    static RED = 0xf25346;
    static WHITE = 0xd8d0d1;
    static BROWN = 0x59332e;
    static PINK = 0xF5986E;
    static BROWNDARK = 0x23190f;
    static BLUE = 0x68c3c0;
    static BLACK = 0x000000;
}



export default Colors;